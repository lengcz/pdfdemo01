package com.it2.pdfdemo01.controller;

import com.it2.pdfdemo01.util.ImageUtil;
import com.it2.pdfdemo01.util.PdfUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/pdftest")
public class MyPdfController {

    /**
     * 在线预览pdf
     *
     * @param request
     * @param response
     * @throws IOException
     */
    @GetMapping("/previewPdf")
    public void previewPdf(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String templateFile = "D:\\test3\\template1.pdf";
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("username", "王小鱼");
        dataMap.put("age", "11");
        dataMap.put("address", "深圳市宝安区和林大道");

        Map<String, byte[]> picMap = new HashMap<>();
        byte[] imageToBytes = ImageUtil.imageToBytes("D:\\test3\\dog3.png");
        picMap.put("head", imageToBytes);

        Map<String, String> checkboxMap = new HashMap<>();
        checkboxMap.put("apple", "Yes");
        checkboxMap.put("orange", "Yes");
        checkboxMap.put("peach", "No");

        response.setCharacterEncoding("utf-8");
        response.setContentType("application/pdf");
        String fileName = new String("测试预览pdf文件".getBytes(), "ISO-8859-1");//避免中文乱码
        response.setHeader("Content-Disposition", "inline;filename=".concat(String.valueOf(fileName) + ".pdf"));
        PdfUtil.output(templateFile, dataMap, picMap, checkboxMap, response.getOutputStream());
    }

    /**
     * 下载pdf
     *
     * @param request
     * @param response
     * @throws IOException
     */
    @GetMapping("/downloadPdf")
    public void downloadPdf(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String templateFile = "D:\\test3\\template1.pdf";
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("username", "王小鱼");
        dataMap.put("age", "11");
        dataMap.put("address", "深圳市宝安区和林大道");

        Map<String, byte[]> picMap = new HashMap<>();
        byte[] imageToBytes = ImageUtil.imageToBytes("D:\\test3\\dog3.png");
        picMap.put("head", imageToBytes);

        Map<String, String> checkboxMap = new HashMap<>();
        checkboxMap.put("apple", "Yes");
        checkboxMap.put("orange", "Yes");
        checkboxMap.put("peach", "No");

        response.setCharacterEncoding("utf-8");
        response.setContentType("application/pdf");
        String fileName = new String("测试预览pdf文件".getBytes(), "ISO-8859-1");//避免中文乱码
        response.setHeader("Content-Disposition", "attachment;filename=".concat(String.valueOf(fileName) + ".pdf"));
        PdfUtil.output(templateFile, dataMap, picMap, checkboxMap, response.getOutputStream());
    }


}