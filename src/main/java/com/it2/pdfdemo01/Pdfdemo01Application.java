package com.it2.pdfdemo01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pdfdemo01Application {

    public static void main(String[] args) {
        SpringApplication.run(Pdfdemo01Application.class, args);
    }

}
