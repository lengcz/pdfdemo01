package com.it2.pdfdemo01.util;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.*;

import java.io.*;
import java.util.List;
import java.util.Map;

/**
 * pdf 工具
 */
public class PdfUtil {


    /**
     * 通过pdf模板输出到流
     *
     * @param templateFile 模板
     * @param dataMap      input数据
     * @param picData      image图片
     * @param checkboxMap  checkbox勾选框
     * @param outputStream 输出流
     */
    public static void output(String templateFile, Map<String, Object> dataMap, Map<String, byte[]> picData, Map<String, String> checkboxMap, OutputStream outputStream) {
        OutputStream os = null;
        PdfStamper ps = null;
        PdfReader reader = null;

        try {
            reader = new PdfReader(templateFile);
            ps = new PdfStamper(reader, outputStream);
            AcroFields form = ps.getAcroFields();
//            BaseFont bf = BaseFont.createFont("Font/SIMYOU.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED); //幼圆常规
            String srcFilePath = PdfUtil.class.getResource("/")+ "Font/simsun.ttc"; //宋体常规
            String templatePath = srcFilePath.substring("file:/".length())+",0";
            BaseFont bf = BaseFont.createFont(templatePath, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

            form.addSubstitutionFont(bf);
            if (null != dataMap) {
                for (String key : dataMap.keySet()) {
                    form.setField(key, dataMap.get(key).toString());
                }
            }
            ps.setFormFlattening(true);
            if (null != checkboxMap) {
                for (String key : checkboxMap.keySet()) {
                    form.setField(key, checkboxMap.get(key), true);
                }
            }

            PdfStamper stamper = ps;
            if (null != picData) {
                picData.forEach((filedName, imgSrc) -> {
                    List<AcroFields.FieldPosition> fieldPositions = form.getFieldPositions(filedName);
                    for (AcroFields.FieldPosition fieldPosition : fieldPositions) {
                        int pageno = fieldPosition.page;
                        Rectangle signrect = fieldPosition.position;
                        float x = signrect.getLeft();
                        float y = signrect.getBottom();
                        byte[] byteArray = imgSrc;
                        try {
                            Image image = Image.getInstance(byteArray);
                            PdfContentByte under = stamper.getOverContent(pageno);
                            image.scaleToFit(signrect.getWidth(), signrect.getHeight());
                            image.setAbsolutePosition(x, y);
                            under.addImage(image);
                        } catch (BadElementException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (DocumentException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                ps.close();
                reader.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 通过pdf模板输出到文件
     *
     * @param templateFile 模板
     * @param dataMap      input数据
     * @param picData      image图片
     * @param checkboxMap  checkbox勾选框
     * @param outputFile   输出流
     */
    public static void output(String templateFile, Map<String, Object> dataMap, Map<String, byte[]> picData, Map<String, String> checkboxMap, File outputFile) throws IOException {
        FileOutputStream fos = new FileOutputStream(outputFile);
        try {
            output(templateFile, dataMap, picData, checkboxMap, fos);
        } finally {
            fos.close();
        }
    }

    /**
     * 通过pdf模板输出到文件
     *
     * @param templateFile 模板
     * @param dataMap      input数据
     * @param picData      image图片
     * @param checkboxMap  checkbox勾选框
     * @param filePath     路径
     * @param fileName     文件名
     */
    public static void output(String templateFile, Map<String, Object> dataMap, Map<String, byte[]> picData, Map<String, String> checkboxMap, String filePath, String fileName) throws IOException {
        File file = new File(filePath + File.separator + fileName);
        output(templateFile, dataMap, picData, checkboxMap, file);
    }

}
